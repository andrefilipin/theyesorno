/**
 * PostsController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  create: function(req, res){
    var params = req.params.all()

    var userId = 0;

    if(req.isSocket == true){
      userId=sails.sockets.id(req.socket);
    }

    if(req.session.passport.user){
      userId=req.session.passport.user;
    }

    Posts.create({text: params.text, bg_color: params.bg_color,user_id: userId, vote: 0, yes: 0, no: 0}).exec(function createCB(err,result){
      if(!err){
        sails.sockets.broadcast('posts','newPost',result);

        return res.json(200,{
          notice: 'Created'
        });
      }else{
        return res.json(500, err);
      }

    });
  },

  index: function(req, res){
    var params = req.params.all()

    if(req.isSocket == true){

      var userId = 0;

      if(req.isSocket == true){
        userId=sails.sockets.id(req.socket);
      }

      if(req.session.passport.user){
        userId=req.session.passport.user;
      }

      sails.sockets.emit(req.socket, 'myId', userId);

      sails.sockets.join(req.socket,'posts');
    }

    Posts.count().exec(function countCB(error, found) {

      var randomId = Math.floor(Math.random() * (found-1));

      Posts.find().skip(randomId).limit(100).exec(function(err,result){
        if(!err){
          return res.json(200,result);
        }else{
          return res.json(500, err);
        }

      });
    });
  },

  my: function(req, res){
    var params = req.params.all();

    var userId = 0;

    if(req.isSocket == true){
      userId=sails.sockets.id(req.socket);
      sails.sockets.leave(req.socket,'posts');
    }

    if(req.session.passport.user){
      userId=req.session.passport.user;
    }

    Posts.find({user_id:userId}).exec(function(err,result){
      if(!err){
        return res.json(200,result);
      }else{
        return res.json(500, err);
      }
    });
  },

  vote: function(req, res){
    var params = req.params.all();

    //var userId = 0;
    //
    //if(req.isSocket == true){
    //  userId=sails.sockets.id(req.socket);
    //  sails.sockets.leave(req.socket,'posts');
    //}
    //
    //if(req.session.passport.user){
    //  userId=req.session.passport.user;
    //}

    Posts.findOneById(params.id).exec(function(err,post){
      if(!err){
        if(params.vote==1){
          post.yes=parseInt(post.yes) + 1;
        }else{
          post.no=parseInt(post.no) + 1;
        }

        post.vote=post.vote + 1;

        Posts.update({id:post.id},{yes:post.yes,no:post.no, vote:post.vote}).exec(function afterwards(err, updated){

          sails.sockets.broadcast('posts','updatePost',updated[0]);

          return res.json(200,updated);
        });

      }else{
        return res.json(500, err);
      }
    });
  }

};

