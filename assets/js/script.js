/**
 * Created by Andre on 01/01/16.
 */

var myId =0;

$(function() {

  io.socket.on('newPost', function (post) {
    createPost(post);
    dragPost('#w' + post.id);
  });

  io.socket.on('myId', function (id) {
    myId = id;
  });

  io.socket.on('updatePost', function (post) {
    updatePost(post);
  });

  showAll();

  $("#panelLeft").mouseenter(function(){
    $('#panelLeft')
      .stop()
      .animate({
        left: -10,
      },300);
  });

  $("#posts").mouseenter(function() {
    $('#panelLeft').stop().animate({left:-($("#panelLeft").width()-50)}, 300);
  });

  $('#postNew').draggable({
    //	handle: "img",
    //scroll: false,
    //stack: "#postNew",
    //cancel: "a.ui-icon", // clicking an icon won't initiate dragging
    revert: "invalid", // when not dropped, the item will revert back to its initial position
    //containment: "document",
    //helper: "clone",
    cursor: "move"
    //helper: 'original',
    //containment: "parent"
  }).bind('click', function(){
    $(this).focus();
  });

  $('#posts').droppable({
    accept: "#postNew",
    drop: function( event, ui ) {

      if($('#postNew').html()!='') {
        savePost($('#postNew').html(), $('#postNew').css('background-color'));
        $('#postNew').html('');
      }

      $(ui.draggable)
        .hide()
        .css({
          "top": -50 + "px",
          "left": 20 + "px"
        })
        .show(100)
        .animate({
          top: 15 + "px",
        }, 100)
        .animate({
          top: 10 + "px",
        }, 100);

    }
  });


  //getPosts();

  $('#colorPicker').simplecolorpicker().change(function(){
    $('#postNew').css('background-color',$(this).val());
  });

  $('#showMy').click(function(){
    showMy();
  });
  $('#showAll').click(function(){
    showAll();
  });

});

function showAll(){
  io.socket.get('/posts',function(result){

    $.each( result, function( key, value ) {
      createPost(value);
      dragPost('#w' + value.id);
    });
  });

  $('#showMy').show();
  $('#showAll').hide();
}


function createPost(post){
  if(myId == post.user_id){
    var yes=post.yes;

    var no=post.no;

    var total=post.vote;

    if(total>0){
      yes= (yes / total) *100;
      no= (no / total) *100;
    }

    return $('#posts').append("<div class='wrapper' id='w" + post.id + "', style='background-color:"+post.bg_color+"'><div class='post'><span class='textPost'>"+post.text+"</span><br><div id='status"+post.id+"'><span class='textVoteStatusLabel'>Yes: </span> <span id='yes" + post.id + "' class='textVoteStatus'>"+yes.toFixed(0)+"%</span><br/><span class='textVoteStatusLabel'>No:</span><span id='no" + post.id + "' class='textVoteStatus'>"+no.toFixed(0)+"%</span><br/><span class='textVoteStatusLabel'>Votes: <span id='total" + post.id + "'>"+total+"</span></span></div></div></div>");
  }else{
    return $('#posts').append("<div class='wrapper' id='w" + post.id + "', style='background-color:"+post.bg_color+"'><div class='post'><span class='textPost'>"+post.text+"</span><br><div id='vote"+post.id+"'><span class='textVote'><a href=\"javascript:votePost('"+post.id+"',1);\">Yes</a></span>&nbsp;&nbsp;  <span class='textPost'>or</span> &nbsp;&nbsp; <span class='textVote'><a href=\"javascript:votePost('"+post.id+"',2);\">No</a></span></div></div></div>");
  }
}


function updatePost(post){

    var yes=post.yes;

    var no=post.no;

    var total=post.vote;

    if(total>0){
      yes= (yes / total) *100;
      no= (no / total) *100;
    }

    $('#yes' + post.id).html(yes.toFixed(0)+'%');
    $('#no' + post.id).html(no.toFixed(0)+'%');
    $('#total' + post.id).html(total);

}


function dragPost(element,posTop,posLeft){

  var newPost=true;
  if(!posTop){
    newPost = false;
    posTop=Math.random() * ($('#posts').height() - $(element).outerHeight());
    posLeft=(Math.random() * ($('#posts').width() - $(element).width()));
  }

  var rotation = Math.random() * 60 - 30;
  //console.log(element);
  $(element)
    .draggable({
      scroll: false,
      stack: ".wrapper",
      helper: 'original',
      containment: "parent",
      cursor: "move"
    }).css({
    "-moz-transform": "rotate(" + rotation + "deg)",
    "-webkit-transform": "rotate(" + rotation + "deg)",
    "-o-transform": "rotate(" + rotation + "deg)",
    "transform": "rotate(" + rotation + "deg)"
  });



  if(newPost){
    $(element).css({
      'position': 'absolute',
      "top": posTop + "px",
      "left": posLeft + "px"
    });
  }else{
    $(element).css({
      'position': 'absolute',
      "top": -20 + "px",
      "left": posLeft + "px"
    }).animate({
        top: posTop+5,
      },300)
      .animate({
        top: posTop,
      },150);
  }

}


function savePost(text,bg_color){

  io.socket.post('/posts/create',{ 'text': text, 'bg_color': bg_color }, function serverResponded (result, JWR) {

  });

}

function showMy(){

  removeAll();

  io.socket.get('/posts/my',function(result){
    if(result){
      $.each( result, function( key, value ) {
        createPost(value);
        dragPost('#w' + value.id);
      });
    }
  });

  $('#showMy').hide();
  $('#showAll').show();

}

function removeAll(){

  $('.wrapper').remove();
  //console.log($('.wrapper').length);
  //$('.wrapper').animate({
  //  top :-500
  //},300,function(){
  //
  //});
}

function votePost(index,vote){

  io.socket.post('/posts/vote',{ 'id': index, 'vote': vote }, function serverResponded (result, JWR) {
    $('#vote'+index).hide(100);

    var yes=result[0].yes;

    var no=result[0].no;

    var total=result[0].vote;

    yes= (yes / total) *100;
    no= (no / total) *100;

    $('#vote'+index).after('<div id="status'+index+'"><span class="textVoteStatusLabel">Yes:</span> <span id="yes' + index + '" class="textVoteStatus">'+yes.toFixed(0)+'%</span><br/><span class="textVoteStatusLabel">No:</span><span id="no' + index+ '" class="textVoteStatus" > '+no.toFixed(0)+'%</span><br/><span class="textVoteStatusLabel">Votes: <span id="total' + index+ '">'+total+'</span></span></div>');


  });
}

var vid = document.getElementById("bgvid");
function vidFade() {
  vid.classList.add("stopfade");
}

vid.addEventListener('ended', function()
{
// only functional if "loop" is removed
  vid.pause();
// to capture IE10
  vidFade();
});

